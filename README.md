# Installation

* Install Dependency pandas: `pip3 install pandas`
* Install Dependency lxml: `pip3 install lxml`
* Install Dependency lxml: `pip3 install openpyxl`

# Konfiguration

## config.json

### Standardbeispiel für die Mapping-Konfiguration "einfacher" Metadaten (inkl. Dublettencheck):
Hier werden den ausgewählten Datensätzen die beiden Felder RestrictionOnAccessLicense und UseAndReproductionLicense hinzugefügt. Es findet ein Dublettencheck statt, d.h. wenn das Metadatum mit exakt dem gleichen Wert bereits existiert, wird nichts geschrieben.

Ideale Konfiguration wenn ein noch nicht bestehendes Metadatum erzeugt oder ein wiederholbares Metadatum angelegt werden soll.

```json
{
    "inputFileName" : "InputFileName.xlsx",
    "metadataDir" : "metadata/",
    "startRow" : 2,
    "endRow" : 300,
    "mapping":
    [
        {
            "ugh": "RestrictionOnAccessLicense",
            "header": "RestrictionOnAccessLicense"
        },
        {
            "ugh": "UseAndReproductionLicense",
            "header": "UseAndReproductionLicense"
        }
    ]
}
```

### Beispiel für das Einfügen "einfacher" Metadaten inklusive Normdaten-Zuweisung
```json
    "mapping":
    [
        {
            "ugh": "Subject",
            "header": "SchlagwortWerk",
            "normDataHeader" : "SchlagwortWerk_GND",
            "normDataAuthority" : "gnd"
        },
        {
            "ugh": "SubjectGeographic",
            "header": "SchlagwortGeo",
            "normDataHeader" : "SchlagwortGeo_GND",
            "normDataAuthority" : "gnd"
        }
    ]
```

### Beispiel für das Überschreiben eines bereits bestehenden Metadatums
Diese Konfiguration soll gewählt werden, wenn das Metadatum mit einem anderen Wert bereits eingetragen ist.

Ideale Konfiguration wenn ein nicht wiederholbares Metadatum durch einen neuen Wert aktualisiert werden soll. (zB `CurrentNoSorting`)

```json
{
    "inputFileName" : "metadata_xls/GasseZiitig_Update.xlsx",
    "metadataDir" : "metadata/",
    "startRow" : 2,
    "endRow" : 2,
    "mapping":
    [
        {
            "ugh": "CurrentNo",
            "header": "CurrentNo",
            "replaceExisting": true
        }
    ]
}
```

Info: Wenn in einem Datensatz ein bestimmtes Metadatum für einige - nicht alle - Vorgänge existiert und ersetzt wird, und bei den anderen Vorgängen hinzugefügt werden muss, empfiehlt sich das Metadata-Update in zwei Durchgängen durchzuführen: Im ersten Laufe mit `replaceExisiting: true` werden die bestehenden Einträge aktualisiert, im zweiten Durchlauf ohne diesen Parameter werden die fehlenden Einträge ergänzt, die zuvor aktualisierten werden Dublett-gechecked und übersprungen.

### Beispiel für abweichenden Einhängepunkt für das neue Metadatum
```json
{
    "inputFileName" : "InputFileName.xlsx",
    "metadataDir" : "metadata/",
    "startRow" : 2,
    "endRow" : 300,
    "mapping":
    [
       {
        "ugh": "QID",
        "header": "QID_Person",
        "xPathFind" : ".//mods:extension/goobi:goobi/goobi:metadata[@name='Balthasar']"
        }
    ]
}
```

#### Setzen von Repräsentatenbildern 
Durch die Modellierung des "Repräsentanten" (oder 'banner') für die Anzeige in Ergebnisansichten als `<goobi:metadata>`-Element kann mittels Skript und abweichendem Einhängepunkt `.//mets:dmdSec[@ID='DMDPHYS_0000']/.//mods:mods/mods:extension/goobi:goobi` solche Bilder im Stapel editiert werden. 

```json
{
    "inputFileName" : "InputFileName.xlsx",
    "metadataDir" : "metadata/",
    "startRow" : 2,
    "endRow" : 300,
    "mapping":
    [
        {
            "ugh": "_representative",
            "header": "Spaltename_mit_Seitenzahl_des_Repraesentanten",
            "xPathFind" : ".//mets:dmdSec[@ID='DMDPHYS_0000']/.//mods:mods/mods:extension/goobi:goobi"
        }
    ]
}
```

### Beispiel Mapping-Config für das Replacement bei Personendaten, basierend auf GND-Matching
```json
        {
            "normDataHeader" : "Person_GND",
            "normDataAuthority" : "gnd",
            "type": "person",
            "ugh" : "SubjectPerson",
            "firstNameHeader" : "Person_Vorname",
            "lastNameHeader" : "Person_Nachname",
            "termsOfAddressHeader" : "Person_Namenszusatz",
            "displayNameHeader" : "Person_Ansetzung", 
            "dateHeader" : "Person_Lebensdaten",              
            "header" : "Person_Ansetzung",
            "replaceExisting": true,
            "xPathFind" : ".//mods:extension/goobi:goobi/goobi:metadata[@name='SubjectPerson']",
            "replaceMatchUGH" : "authorityValue",
            "replaceMatchHeader" : "Person_GND"
        }
```

### Beispiel Mapping-Config für das Hinzufügen neuer Personendaten
```json
        {
            "normDataHeader" : "Person_QID",
            "normDataAuthority" : "wikidata",
            "type": "person",
            "ugh" : "Creator",
            "firstNameHeader" : "Person_Vorname",
            "lastNameHeader" : "Person_Nachname",
            "termsOfAddressHeader" : "Person_Namenszusatz",
            "displayNameHeader" : "Person_Ansetzung", 
            "dateHeader" : "Person_Lebensdaten",              
            "header" : "Person_Ansetzung"
        }   
```

### Beispiel Mapping-Config für das Hinzufügen von Personendaten mit Splitting-Character
```json
        {
            "ugh": "Composer",
            "header" : "Komponist",
            "type" : "person",
            "splitCharacter" : ",",
            "displayNameHeader" : "Komponist"
        }    
```

### Beispiel Mapping-Config für das Hinzufügen neuer Körperschaften
```json
        {
            "normDataHeader" : "CorpGND",
            "normDataAuthority" : "gnd",
            "type": "corporate",
            "ugh" : "CorporateAuthor",
            "mainNameHeader" : "mainName",
            "subNameHeader" : "subName",
            "header" : "mainName"
        } 
```


### Beispiel für das Ändern des DocStrctTypes
```json
    "mapping":
    [
        {
            "header": "DocStrctType",
            "replaceExisting": true,
            "type" : "DocStrctType",
            "xPathFind" : ".//mets:div[@DMDID='DMDLOG_0000']"
        }
    ]
```

### Beispiel für das Löschen eines Metadatums
Ein Metadatum wird nur gelöscht, wenn auch der zu löschende Wert mitübergeben wird. (Ein einfaches "Lösche alle Metadaten des Metadatums XY ohne Wert ist nicht möglich!)
```json
    "mapping":
    [
        {
            "ugh": "ARK",
            "header": "ARK",
            "deleteMetadata": true
        }
    ]
```

# Postproduction in Goobi

```yaml
---
# This GoobiScript ensures that the internal database table of the Goobi database is updated with the status of the workflows and the associated media files as well as metadata from the METS file.
action: updateDatabaseCache

```