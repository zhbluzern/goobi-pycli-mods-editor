import src.addMods as Mods
import lxml.etree as ET
from lxml import etree

def getDMDLOG_ID(dmd,dmdlogN):
    if dmd != False:
        dmdlog = f"DMDLOG_{str(dmdlogN).zfill(4)}"
        dmdlogN += 1
    else:
        dmdlog = None
    return dmdlog, dmdlogN


def addMETSStrctMap(mytree, docStrctType, logId, dmdLogId=None, xpathFindStr=""):
    xpathDupl = f"{xpathFindStr}/mets:div[@ID='{logId}']"
    metsDupl = Mods.findMetadata(mytree, xpathStr=xpathDupl)
    if metsDupl == []:
        print(f"mets:div with LOGID {logId} not found - add new one")
        newMetadata = Mods.getNewMetadataElement(mytree, metadata='{http://www.loc.gov/METS/}div', xpathFindStr=xpathFindStr)
        #<mets:div DMDID="DMDLOG_0000" ID="LOG_0000" TYPE="Object"/>
        if dmdLogId != None:
            newMetadata.set("DMDID", dmdLogId)
        newMetadata.set("ID", logId)
        newMetadata.set("TYPE", docStrctType)
    else:
        print(f"mets:div with LOGID {logId} already found")
    return mytree

def addStructLink(mytree, physId, logId):
    xpathFindStr = ".//mets:structLink"
    xpathDupl = f"{xpathFindStr}/mets:smLink[@xlink:from='{logId}'][@xlink:to='{physId}']"
    metsDupl = Mods.findMetadata(mytree, xpathStr=xpathDupl)
    
    if metsDupl == []:
        print(f"::add mets:smLink {logId} to {physId}")
        newMetadata = Mods.getNewMetadataElement(mytree, metadata='{http://www.loc.gov/METS/}smLink', xpathFindStr=xpathFindStr)
        newMetadata.set("{http://www.w3.org/1999/xlink}to", physId)
        newMetadata.set("{http://www.w3.org/1999/xlink}from", logId)
    else:
        print(f"::mets:smLink with LOGID {logId} and PHYS {physId} already set")
    return mytree

def setOrderLabel(mytree, physId, orderlabel):
    #  <mets:structMap TYPE="PHYSICAL">
    #   <mets:div DMDID="DMDPHYS_0000" ID="PHYS_0000" TYPE="BoundBook">
    #      <mets:div ID="PHYS_0001" ORDER="1" ORDERLABEL="uncounted" TYPE="page">
    xpathFindStr = f".//mets:structMap[@TYPE='PHYSICAL']//mets:div[@ID='{physId}']"
    metsTarget = Mods.findMetadata(mytree, xpathStr=xpathFindStr)
    metsTarget[0].attrib["ORDERLABEL"] = orderlabel
    return mytree

def addNewDMD(mytree, dmdLogId, prevNode):
    namespaces = {'mods' : 'http://www.loc.gov/mods/v3','mets' : 'http://www.loc.gov/METS/',  'goobi' :  'http://meta.goobi.org/v1.5.1/' }
    xpathDupl = f".//mets:dmdSec[@ID='{dmdLogId}']"
    metsDupl = Mods.findMetadata(mytree, xpathStr=xpathDupl)
    if metsDupl == []:
        print(f"DMDSEC with {dmdLogId} created")
        newDmdNode = createNewDMD(dmdLogId)
        xpathFindStr = f".//mets:dmdSec[@ID='{prevNode}']"
        target_node = mytree.find(xpathFindStr, namespaces)
        target_node.addnext(newDmdNode)
    else:
        print(f"DMDSEC with {dmdLogId} already exists")
    return mytree
    
def createNewDMD(dmdLogId):
    namespaces = {'mods' : 'http://www.loc.gov/mods/v3','mets' : 'http://www.loc.gov/METS/',  'goobi' :  'http://meta.goobi.org/v1.5.1/' }
    ET.register_namespace=namespaces
    metsDmd = ET.Element('{http://www.loc.gov/METS/}dmdSec')
    metsDmd.set("ID", dmdLogId)
    metsmdWrap = ET.Element('{http://www.loc.gov/METS/}mdWrap')
    metsmdWrap.set("MDTYPE","MODS")
    metsXML = ET.Element('{http://www.loc.gov/METS/}xmlData')
    modsMods = ET.Element('{http://www.loc.gov/mods/v3}mods')
    modsExt= ET.Element('{http://www.loc.gov/mods/v3}extension')
    goobiNode = ET.Element('{http://meta.goobi.org/v1.5.1/}goobi')
    modsExt.append(goobiNode)
    modsMods.append(modsExt)
    metsXML.append(modsMods)
    metsmdWrap.append(metsXML)
    metsDmd.append(metsmdWrap)
    #pretty_xml = ET.tostring(metsDmd, pretty_print=True, encoding='UTF-8').decode()
    #print(pretty_xml)
    return metsDmd

def addFileSec(mytree, fileLink):
    namespaces = {'mods' : 'http://www.loc.gov/mods/v3','mets' : 'http://www.loc.gov/METS/',  'goobi' :  'http://meta.goobi.org/v1.5.1/', 'xlink' : 'http://www.w3.org/1999/xlink/' }
    prevSibling = mytree.find(".//mets:dmdSec[last()]", namespaces)
    # Create the <mets:fileSec> element
    metsFileSec = etree.Element("{http://www.loc.gov/METS/}fileSec")
    # Create the <mets:fileGrp> element
    metsfileGrp = etree.SubElement(metsFileSec, "{http://www.loc.gov/METS/}fileGrp")
    metsfileGrp.set("USE", "LOCAL")
    # Create the <mets:file> element
    metsFile = etree.SubElement(metsfileGrp, "{http://www.loc.gov/METS/}file", nsmap={'xlink': namespaces['xlink']})
    metsFile.set("ID", "FILE_0001")
    metsFile.set("MIMETYPE", "video/mp4")
    # Create the <mets:FLocat> element
    metsFLocat = etree.SubElement(metsFile, "{http://www.loc.gov/METS/}FLocat")
    metsFLocat.set("LOCTYPE", "URL")
    metsFLocat.set("{http://www.w3.org/1999/xlink/}href", fileLink)
    prevSibling.addnext(metsFileSec)

def addPhysMetsfptr(mytree, targetNode):
    namespaces = {'mods' : 'http://www.loc.gov/mods/v3','mets' : 'http://www.loc.gov/METS/',  'goobi' :  'http://meta.goobi.org/v1.5.1/', 'xlink' : 'http://www.w3.org/1999/xlink/' }
    metsDiv = etree.Element("{http://www.loc.gov/METS/}div")
    metsDiv.set("ID","PHYS_0001")
    metsDiv.set("ORDER", "1")
    metsDiv.set("ORDERLABEL", "uncounted")
    metsDiv.set("TYPE", "page")

    metsfptr = etree.SubElement(metsDiv, "{http://www.loc.gov/METS/}fptr")
    metsfptr.set("FILEID", "FILE_0001")
    print(metsDiv)
    targetNode[0].append(metsDiv)

def addmetsStructLink(mytree):
    namespaces = {'mods' : 'http://www.loc.gov/mods/v3','mets' : 'http://www.loc.gov/METS/',  'goobi' :  'http://meta.goobi.org/v1.5.1/', 'xlink' : 'http://www.w3.org/1999/xlink/' }
    metsStructLink = etree.Element("{http://www.loc.gov/METS/}structLink",nsmap={'xlink': namespaces['xlink']})
    metssmLink = etree.SubElement(metsStructLink, "{http://www.loc.gov/METS/}smLink" )
    metssmLink.set("{http://www.w3.org/1999/xlink/}to","PHYS_0001")
    metssmLink.set("{http://www.w3.org/1999/xlink/}from","LOG_0000")
    return metsStructLink