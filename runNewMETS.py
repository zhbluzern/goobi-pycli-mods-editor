import pandas as pd
import json
import src.addMods as Mods
import src.MetsHelper as Mets
import re

# read and parse ConfigFile
with open('config.json', 'r') as myfile:
    data=myfile.read()
config = json.loads(data)
#print(config)
# metadataFolder-Path
metadataDir = config["metadataDir"]

#inputFileName = input("Bitte Input-XLS-Dateinamen (inkl. Endung) angeben: ")
inputFileName = config["inputFileName"]
data = pd.read_excel(inputFileName)
#df = pd.DataFrame(data, columns=['VorgangID','UseAndReproductionLicense'])
df = pd.DataFrame(data)
df.fillna("", inplace=True)
df = df.iloc[(config["startRow"]-2):(config["endRow"]-1)]
print (df)

processId_Compare = None
paginator_Compare = None
#Schleife durch das Excel-Input-File, konkret den pandaisierten DataFrame.
for index, row in df.iterrows():
    processId = str(row[config["processIdHeader"]])
    if processId_Compare != processId:
        print(f"===========================================")
        print(f"New Volume with ID: {processId}")
        processId_Compare = processId
        dmdlogN = 2
        #Einlesen des meta(_anchor).xml-Files für den angesprochenen Vorgang
        filename = f"{metadataDir}{processId}/{config['metaFile']}.xml"
        modsTree = Mods.parseMetadata(filename)
        Mods.backupMetaFile(processId,modsTree,metadataDir,config['metaFile'])

    #1. Step - Add new mets:div in mets:structMap
    dmdlog = Mets.getDMDLOG_ID(config["docStrctMapping"][row["docStrctType"]].get("dmd"),dmdlogN)
    dmdlogN = dmdlog[1]
    print(f"add type: {config['docStrctMapping'][row['docStrctType']]['ugh']} with {row['LOG_ID']} {dmdlog[0]}")
    Mets.addMETSStrctMap(modsTree, config['docStrctMapping'][row['docStrctType']]['ugh'], row['LOG_ID'], dmdlog[0], xpathFindStr=".//mets:structMap[@TYPE='LOGICAL']//mets:div[@DMDID='DMDLOG_0001']")

    #2. Step - Add links from physical images to logical pages and orderlabel in structMap Physical
    print(f"add physical pages between {row['startImage']} and {row['endImage']} linked to {row['LOG_ID']}")
    page = int(row["startImage"])
    paginator = row.get("ORDERLABEL")
    if paginator != None and paginator != "":
        paginator = int(paginator)
    print(f"add page label starting with {paginator}")
    while page <= int(row["endImage"]):
        #2.1 Add links from physical images to logical pages
        Mets.addStructLink(modsTree, f"PHYS_{str(page).zfill(4)}", row['LOG_ID'])
        #2.2 Change ORDERLABEL in structMap Physical
        if paginator != None and paginator != "":
            orderlabel = str(paginator)
            paginator += 1
        else:
            orderlabel = "uncounted"
        print (f"phys page PHYS_{str(page).zfill(4)} has label {orderlabel}")
        Mets.setOrderLabel(modsTree,f"PHYS_{str(page).zfill(4)}",orderlabel)
        page +=1

    #3. Step - Add DMDLOG with Metadata
    if dmdlog[0] != None:
        prevNode = f"DMDLOG_{str(dmdlog[1]-2).zfill(4)}"
        print(f"** add Metadata block for {dmdlog[0]} (prev-node: {prevNode} ) with TitleDocMain: {row['dc:title_0']} von {row['dc:creator_0']} (DOI: {row['DOI']})")
        Mets.addNewDMD(modsTree, dmdlog[0], prevNode)
        #Add Metadata to new dmdSec-Node: title, titleSort, creator*, language=ger, doi
        for configData in config["mapping"]:
            noReplaceExistingMods = False
            if row[configData["header"]] == "": #Wenn Datenzelle leer, dann weiter zum naechsten Metadatum
                continue
            else:
                print(f"Add metadata {configData['ugh']} with {row[configData['header']]}")
                xpathFindStr = f".//mets:dmdSec[@ID='{dmdlog[0]}']//goobi:goobi"
                mdType = configData.get("type")
                if mdType == "person":
                    modsTree = Mods.addPersonOrCorpMetadata(modsTree, row, configData, xpathFindStr=xpathFindStr )
                else:
                    if configData["ugh"] == "DOI":
                        mdValue = re.sub("doi:","",row[configData['header']])
                    else:
                        mdValue = row[configData['header']]
                    Mods.addMetadata(modsTree, value=mdValue, attributeName=configData["ugh"], xpathFindStr=xpathFindStr )
                if configData["ugh"] == "TitleDocMain":
                    sortTitle = Mods.getSortTitle(row[configData['header']])
                    print(f"Add metadata TitleDocMainShort with {sortTitle}")
                    Mods.addMetadata(modsTree, value=sortTitle, attributeName="TitleDocMainShort", xpathFindStr=xpathFindStr )
                    if config["docStrctMapping"][row["docStrctType"]]["ugh"] == "Article":
                        Mods.addMetadata(modsTree, value="ger", attributeName="DocLanguage", xpathFindStr=xpathFindStr )

    #Write Changes to meta*.xml
    Mods.writeNewMetaFile(processId,modsTree,metadataDir,config['metaFile'])